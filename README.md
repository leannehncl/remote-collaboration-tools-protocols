# README #

This is an open source list of remote work and collaboration tools that might be handy
during (and after) the COVID19 crisis

### What we use at [ICOS](https://ico2s.org) ###

* [Slack](https://www.slack.com): we have set up a group workspace, containing multiple different, channels to capture the usual chats that happen in a (shared) office and share some files, etc.
* [Trello](https://www.trello.com): we use it to have a set of cards and list of work in progress (integrates nicely with Bitbucket and Jira), mostly for programming projects (and integrated with Bitbucket)
* [Whereby](https://www.whereby.com): enables you to set up video chat rooms (VCRs) via the browser that you can keep open while working. Complements the short/fast messages in slack with deeper (when needed) discussions.
* [Miro](https://www.miro.com): to create collaborative on-line whiteboards with your team (integrates with Jira, Asana, etc)
* [ChuChu](https://chuchu-app.herokuapp.com): allows you to create recurrent collaboration cycles between your team members. Notifies only on a need-to-know basis thus reducing overheads. Keeps all files, notes, etc associated with the various project cycles (hence no need to search emails, dropbox, etc).
* [Asana](https://www.asana.com): super-powered "to do" lists and integrations. Also has "cards" like Trello.
* [Basecamp](https://www.basecamp.com): keeps a distributed team up to date with work and maintains all files in the same place (similarly to ChuChu)
* [Bitbucket](https://www.Bitbucket.com), [Github](https://www.Github.com), [Gitlab](https://www.gitlab.com): for version control of code and other stuff (like this list)
* [Whatsapp web](https://web.whatsapp.com): enables you to create a session linking your whatsapp account from your phone with your browser so you can more readily write and work (its difficult to type long stuff in a phone!)
